from channels.auth import AuthMiddlewareStack
from channels.routing import ProtocolTypeRouter, URLRouter

import resumeapp.routing

application = ProtocolTypeRouter({

    'websocket': AuthMiddlewareStack(
        URLRouter(
            resumeapp.routing.websocket_urlpatterns
        )
    )
})
