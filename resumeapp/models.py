from djongo import models
from django import forms

# Create your models here.

MALE = 'Male'
FEMALE = 'Female'
MALE_ID = 1
FEMALE_ID = 2

GENDER = (
    (MALE_ID, MALE),
    (FEMALE_ID, FEMALE)
)


RATING_1 = 1
RATING_2 = 2
RATING_3 = 3
RATING_4 = 4
RATING_5 = 5

RATING = (
    (RATING_1, "1"),
    (RATING_2, "2"),
    (RATING_3, "3"),
    (RATING_4, "4"),
    (RATING_5, "5"),
)


class Contact(models.Model):

    address = models.CharField(max_length=100)
    contact_number = models.CharField(max_length=30)
    email = models.EmailField()

    class Meta:
        abstract = True

    def __str__(self):
        return self.address


class Education(models.Model):

    name = models.CharField(max_length=50)
    date_from = models.DateField()
    date_to = models.DateField(null=True)

    class Meta:
        abstract = True

    def __str__(self):
        return self.name


class Expertise(models.Model):

    name = models.CharField(max_length=50)
    rating = models.PositiveSmallIntegerField(choices=RATING)

    class Meta:
        abstract = True

    def __str__(self):
        return self.name


class Skill(models.Model):

    name = models.CharField(max_length=50)
    rating = models.PositiveSmallIntegerField(choices=RATING)

    class Meta:
        abstract=True

    def __str__(self):
        return self.name


class Achievement(models.Model):

    description = models.TextField()

    class Meta:
        abstract = True

    def __str__(self):
        return self.description


class WorkExperience(models.Model):

    date_from = models.DateField()
    date_to = models.DateField(null=True)
    occupation = models.CharField(max_length=50)
    company = models.CharField(max_length=50)
    place = models.CharField(max_length=100)
    description = models.TextField()
    achievements = models.ArrayModelField(
        model_container=Achievement,
        null=True
    )

    class Meta:
        abstract = True

    def __str__(self):
        return '%s at %s' % (self.occupation, self.company)


class TechnicalProficiency(models.Model):

    name = models.CharField(max_length=50)

    class Meta:
        abstract = True

    def __str__(self):
        return self.name

class Hobby(models.Model):

    name = models.CharField(max_length=50)

    class Meta:
        abstract = True

    def __self__(self):
        self.name

class Interest(models.Model):

    name = models.CharField(max_length=50)

    class Meta:
        abstract = True

    def __str__(self):
        self.name


class Resume(models.Model):

    career_objective = models.TextField()
    expertise = models.ArrayModelField(
        model_container=Expertise,
        null=True
    )
    skills = models.ArrayModelField(
        model_container=Skill,
        null=True
    )
    contact = models.EmbeddedModelField(
        model_container=Contact,
        null=True
    )
    # contacts = models.ArrayModelField(
    #     model_container=Contact,
    #     null=True
    # )
    education = models.ArrayModelField(
        model_container=Education,
        null=True
    )
    work_experiences = models.ArrayModelField(
        model_container=WorkExperience,
        null=True
    )
    technical_proficiencies = models.ArrayModelField(
        model_container=TechnicalProficiency,
        null=True
    )
    hobbies = models.ArrayModelField(
        model_container=Hobby,
        null=True
    )
    interests = models.ArrayModelField(
        model_container=Interest,
        null=True
    )

    class Meta:
        abstract = True

    def __str__(self):
        return self.career_objective


class Person(models.Model):

    objects = models.DjongoManager()

    _id = models.ObjectIdField()
    first_name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50)
    occupation = models.CharField(max_length=50)
    date_of_birth = models.DateField()
    age = models.PositiveSmallIntegerField()
    gender = models.PositiveSmallIntegerField(choices=GENDER)
    birth_place = models.CharField(max_length=100)

    resume = models.EmbeddedModelField(
        model_container=Resume,
        null=True
    )

    def __str__(self):
        return self.first_name
