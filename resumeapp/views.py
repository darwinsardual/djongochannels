from datetime import date
from bson.objectid import ObjectId

from django.shortcuts import render
from .models import Person, Resume, WorkExperience, Education, Interest, Hobby, TechnicalProficiency, Skill, Expertise, Contact, Achievement

# Create your views here.

def index(request):

    contact = Contact(
        address='Matina, Davao city',
        contact_number='09199798176',
        email='darwin.sardual@gmail.com'
    )

    contact1 = Contact(
        address='Panabo City',
        contact_number='09199798177',
        email='darwin.sardual@gmail.com'
    )

    education = Education(
        name='Bachelor of Science in Computer Science',
        date_from=date.today(),
        date_to=date.today()
    )

    education1 = Education(
        name='Bachelor of Science in Information Technology',
        date_from=date.today(),
        date_to=date.today()
    )

    website_achievement = Achievement(
        description='Create a website'
    )

    mobile_achievement = Achievement(
        description='Create a mobile app'
    )

    work_experience = WorkExperience(
        occupation='Web developer',
        date_from=date.today(),
        date_to=date.today(),
        company='Eversun',
        place='Bolton St, Davao City',
        description='Main web developer',
        achievements=[website_achievement, mobile_achievement]
    )

    work_experience1 = WorkExperience(
        occupation='Graphics Designer',
        date_from=date.today(),
        date_to=date.today(),
        company='Alta',
        place='Pampanga',
        description='Main web developer',
        achievements=[website_achievement]
    )

    javascipt_proficiency = TechnicalProficiency(
        name='Javscript'
    )

    html_proficiency = TechnicalProficiency(
        name='HTML'
    )

    css_proficiency = TechnicalProficiency(
        name='CSS'
    )


    eating_hobby = Hobby(
        name='Eating'
    )

    sleeping_hobby = Hobby(
        name='Sleeping'
    )

    games_interest = Interest(
        name='Games'
    )

    books_interest = Interest(
        name='Books'
    )

    problem_solving_skill = Skill(
        name='Problem Solving',
        rating=4
    )

    critical_thinker_skill = Skill(
        name='Critical Thinker',
        rating=5
    )

    cms_expertise = Expertise(
        name='CMS customization',
        rating=4
    )

    widget_expertise = Expertise(
        name='Widget development',
        rating=3
    )

    resume = Resume(
        career_objective='To become on of the best employee',
        expertise=[cms_expertise],
        skills=[problem_solving_skill],
        contact=contact,
        education=[education],
        work_experiences=[work_experience1],
        technical_proficiencies=[html_proficiency, javascipt_proficiency],
        hobbies=[sleeping_hobby],
        interests=[books_interest]
    )

    # resume.save()

    resume1 = Resume(
        career_objective='To become on of the best employee',
        expertise=[cms_expertise, widget_expertise],
        skills=[problem_solving_skill, critical_thinker_skill],
        contact=contact,
        education=[education, education1],
        work_experiences=[work_experience, work_experience1],
        technical_proficiencies=[html_proficiency, javascipt_proficiency, css_proficiency],
        hobbies=[sleeping_hobby, eating_hobby],
        interests=[books_interest, games_interest]
    )
    # resume1.save()
    # resume.technical_proficiencies.add(html_proficiency)

    person = Person(
        first_name='Darwin',
        last_name='Sardual',
        occupation='Web Developer',
        date_of_birth=date.today(),
        age=24,
        gender=1,
        birth_place='Davao City',
        resume=resume
    )

    person1 = Person(
        first_name='Erick',
        last_name='Gonzaga',
        occupation='Database admin',
        date_of_birth=date.today(),
        age=30,
        gender=1,
        birth_place='Gensan',
        resume=resume1
    )




    # Person.objects.mongo_update(
    #     {'_id': ObjectId('5bd1f4a9ea34c06a44d53753')},
    #     # {'$addToSet': {'resumes.0.technical_proficiencies': {'name': 'CSS'}}}
    # )

    # person = Person.objects.get(_id=ObjectId('5bd1f4a9ea34c06a44d53753'))
    # person.resumes.add(resume)
    # person.save()

    # person.resumes.add(resume)

    # person.save()

    # person1.resumes.add(resume)
    # person1.resumes.add(resume1)
    person1.save()
    return render(request, 'resumeapp/index.html', {})
