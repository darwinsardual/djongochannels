from datetime import date

from rest_framework import serializers
from bson.objectid import ObjectId

from .models import Person, Education, Contact, Resume, WorkExperience, Achievement, Skill


class SkillSerializer(serializers.Serializer):

    name = serializers.CharField(max_length=50)
    rating = serializers.IntegerField()

    def update(self, instance, validated_data):

        person_id = self.context.get('person_id')
        name = validated_data.get('name')
        rating = validated_data.get('rating')

        instance.rating = rating

        person = Person.objects.get(_id=ObjectId(person_id))
        person.resume.skills[self.context.get('index')] = instance
        person.save()

        return instance


class ExpertiseSerializer(serializers.Serializer):

    name = serializers.CharField(max_length=50)
    rating = serializers.IntegerField()

    def update(self, instance, validated_data):

        person_id = self.context.get('person_id')
        name = validated_data.get('name')
        rating = validated_data.get('rating')

        instance.rating = rating

        person = Person.objects.get(_id=ObjectId(person_id))
        person.resume.expertise[self.context.get('index')] = instance
        person.save()

        return instance


class AchievementSerializer(serializers.Serializer):

    description = serializers.CharField()


class WorkExperienceSerializer(serializers.Serializer):

    date_from = serializers.DateField()
    date_to = serializers.DateField()
    occupation = serializers.CharField(max_length=50)
    company = serializers.CharField(max_length=50)
    place = serializers.CharField(max_length=100)
    description = serializers.CharField()

    achievements = AchievementSerializer(many=True)

    def create(self, validated_data):

        achievements = []
        for achievement in validated_data.get('achievements'):
            achievements.append(Achievement(
                description=achievement.get('description')
            ))

        work_experience = WorkExperience(
            date_from=validated_data.get('date_from'),
            date_to=validated_data.get('date_to'),
            occupation=validated_data.get('occupation'),
            company=validated_data.get('company'),
            place=validated_data.get('place'),
            description=validated_data.get('description'),
            achievements=achievements
        )

        person = Person.objects.get(_id=ObjectId(self.context.get('person_id')))
        person.resume.work_experiences.append(work_experience)
        person.save()

        return work_experience

    def validate(self, data):
        if data['date_from'] >= data['date_to']:
            raise serializers.ValidationError("date from must no be greater or equal to date to")

        return data


class EducationSerializer(serializers.Serializer):

    name = serializers.CharField(max_length=50)
    date_from = serializers.DateField()
    date_to = serializers.DateField()

    def create(self, validated_data):

        education = Education(
            name=validated_data.get('name'),
            date_from=validated_data.get('date_from'),
            date_to=validated_data.get('date_to')
        )

        person = Person.objects.get(_id=ObjectId(self.context.get('person_id')))
        person.resume.education.append(education)
        person.save()

        return education


class TechnicalProficiencySerializer(serializers.Serializer):

    name = serializers.CharField(max_length=50)


class TechnicalProficiencyPersonsSerializer(serializers.Serializer):

    _id = serializers.CharField()
    first_name = serializers.CharField(max_length=50)
    last_name = serializers.CharField(max_length=50)


class ContactSerializer(serializers.Serializer):

    address = serializers.CharField(max_length=100)
    contact_number = serializers.CharField(max_length=30)
    email = serializers.EmailField()

    def create(self, validated_data):

        contact = Contact(
            address=validated_data.get('address'),
            contact_number=validated_data.get('contact_number'),
            email=validated_data.get('email')
        )

        person = Person.objects.get(_id=ObjectId(self.context.get('person_id')))
        person.resume.contact = contact
        person.save()

        return contact

    def update(self, instance, validated_data):

        instance.address = validated_data.get('address')
        instance.contact_number = validated_data.get('contact_number')
        instance.email = validated_data.get('email')

        person = Person.objects.get(_id=ObjectId(self.context.get('person_id')))
        person.resume.contact = instance
        person.save()

        return instance

class PersonSerializer(serializers.ModelSerializer):

    class Meta:
        model = Person
        fields = ('first_name', 'last_name', 'occupation', 'date_of_birth', 'age', 'gender', 'birth_place')

    def create(self, validated_data):

        resume = Resume(
            career_objective='Empty resume',
            expertise=[],
            skills=[],
            contacts=[],
            education=[],
            work_experiences=[],
            technical_proficiencies=[],
            hobbies=[],
            interests=[]
        )


        person = Person(
            first_name=validated_data.get('first_name'),
            last_name=validated_data.get('last_name'),
            occupation=validated_data.get('occupation'),
            date_of_birth=validated_data.get('date_of_birth'),
            age=validated_data.get('age'),
            gender=validated_data.get('gender'),
            birth_place=validated_data.get('birthplace'),
            resume=resume
        )
        person.save()

        return person
