import json

from asgiref.sync import async_to_sync
from channels.generic.websocket import WebsocketConsumer


class BaseConsumer(WebsocketConsumer):

    # self.room_group_name must be defined on the child class
    room_group_name = ''

    def connect(self):
        async_to_sync(self.channel_layer.group_add)(
            self.room_group_name,
            self.channel_name,
        )
        self.accept()

    def disconnect(self, close_code):
        async_to_sync(self.channel_layer.group_discard)(
            self.room_group_name,
            self.channel_name,
        )

    def receive(self, text_data):
        converted_text_data = json.loads(text_data)
        message = converted_text_data['message']
        type = '%s_%s' % (self.room_group_name, message['action'])
        data = message['data']

        async_to_sync(self.channel_layer.group_send)(
            self.room_group_name,
            {
                'type': type,
                'action': message['action'],
                'data': data
            }
        )

    def send_data(self, action, data):

        self.send(
            text_data=json.dumps({
                'message': {
                    'action': action,
                    'data': data
                }
            })
        )
