from datetime import datetime
from datetime import date
import json
from django.core.serializers.json import DjangoJSONEncoder

from .base_consumer import BaseConsumer
from bson.objectid import ObjectId

from resumeapp.models import Person, Education, Contact, Resume, Skill, Expertise
from resumeapp.serializers import WorkExperienceSerializer, EducationSerializer, TechnicalProficiencySerializer, ContactSerializer, PersonSerializer, TechnicalProficiencyPersonsSerializer, SkillSerializer, ExpertiseSerializer

class ResumeConsumer(BaseConsumer):

    room_group_name = 'resume'

    def find_skill_or_expertise(self, lst, name):
        index = 0
        for item in lst:
            if item.name == name:
                return index

            index += 1

        return None

    def resume_list_work_experiences(self, event):
        action = event['action']
        data = event['data']

        person = Person.objects.get(_id=ObjectId(data['id']))
        serializer = WorkExperienceSerializer(
            person.resume.work_experiences,
            many=True
        )
        serialized_data = json.dumps(serializer.data, cls=DjangoJSONEncoder)
        self.send_data(action, serialized_data)

    def resume_list_education(self, event):
        action = event['action']
        data = event['data']

        person = Person.objects.get(_id=ObjectId(data['id']))
        serializer = EducationSerializer(
            person.resume.education,
            many=True
        )

        serialized_data = json.dumps(serializer.data, cls=DjangoJSONEncoder)
        self.send_data(action, serialized_data)

    def resume_list_technical_proficiences(self, event):
        action = event['action']
        data = event['data']

        person = Person.objects.get(_id=ObjectId(data['id']))
        serializer = TechnicalProficiencySerializer(
            person.resume.technical_proficiencies,
            many=True
        )

        serialized_data = json.dumps(serializer.data, cls=DjangoJSONEncoder)
        self.send_data(action, serialized_data)

    def resume_list_contacts(self, event):
        action = event['action']
        data = event['data']

        person = Person.objects.get(_id=ObjectId(data['id']))

        serializer = ContactSerializer(
            person.resume.contacts,
            many=True
        )

        serialized_data = json.dumps(serializer.data, cls=DjangoJSONEncoder)
        self.send_data(action, serialized_data)


    def resume_list_person_information(self, event):
        action = event['action']
        data = event['data']

        person = Person.objects.get(_id=ObjectId(data['id']))
        serializer = PersonSerializer(
            person
        )

        serialized_data = json.dumps(serializer.data, cls=DjangoJSONEncoder)
        self.send_data(action, serialized_data)

    def resume_list_technical_proficiency_persons(self, event):
        action = event['action']
        data = event['data']

        persons = Person.objects.filter(resume={'technical_proficiencies': {'name': data['proficiency']}})

        serializer = TechnicalProficiencyPersonsSerializer(
            persons,
            many=True
        )

        serialized_data = json.dumps(serializer.data, cls=DjangoJSONEncoder)
        self.send_data(action, serialized_data)

    def resume_add_education(self, event):
        action = event['action']
        data = event['data']

        education_serializer = EducationSerializer(
            data={
                'name': data['name'],
                'date_from': data['date_from'],
                'date_to': date['date_to']
            },
            context={
                'person_id': data['id']
            }
        )

        if education_serializer.is_valid():
            education_serializer.save()
        else:
            print(education_serializer.errors)

    def resume_add_work_experience(self, event):
        action = event['action']
        data = event['data']

        work_experience_serializer = WorkExperienceSerializer(
            data={
                'date_from': data['date_from'],
                'date_to': data['date_to'],
                'occupation': data['occupation'],
                'company': data['company'],
                'place': data['place'],
                'description': data['description'],
                'achievements': data['achievements']
            },
            context={
                "person_id": data['id']
            }
        )

        if work_experience_serializer.is_valid():
            work_experience_serializer.save(raise_exception=True)
        else:
            print(work_experience_serializer.errors)

    def resume_add_contact(self, event):
        action = event['action']
        data = event['data']

        contact_serializer = ContactSerializer(
            data={
                'address': data['address'],
                'contact_number': data['contact_number'],
                'email': data['email']
            },
            context={
                'person_id': data['id']
            }
        )

        if contact_serializer.is_valid():
            contact_serializer.save()
        else:
            print(contact_serializer.errors)


    def resume_update_contact(self, event):
        action = event['action']
        data = event['data']

        person = Person.objects.get(_id=ObjectId(data['id']))
        contact = Contact(
            address=person.resume.contact.address,
            contact_number=person.resume.contact.contact_number,
            email=person.resume.contact.email,
        )

        contact_serializer = ContactSerializer(
            instance=contact,
            data={
                'address': data['address'],
                'contact_number': data['contact_number'],
                'email': data['email']
            },
            context={
                'person_id': data['id']
            },
            partial=True
        )

        if contact_serializer.is_valid():
            contact_serializer.save()
        else:
            print(contact_serializer.errors)


    def resume_add_person(self, event):
        action = event['action']
        data = event['data']

        person_serializer = PersonSerializer(
            data={
                'first_name': data['first_name'],
                'last_name': data['last_name'],
                'occupation': data['occupation'],
                'date_of_birth': data['date_of_birth'],
                'age': data['age'],
                'gender': data['gender'],
                'birth_place': data['birth_place'],

            }
        )

        if person_serializer.is_valid():
            person_serializer.save()
        else:
            print(person_serializer.errors)


    def resume_update_skill_rating(self, event):
        action = event['action']
        data = event['data']

        Person.objects.mongo_update_one(
            {
                '_id': ObjectId(data['id']),
                'resume.skills.name': data['name']
            },
            {
                '$set': {
                    "resume.skills.$.rating": data['rating']
                }
            }
        )


    def resume_delete_skill(self, event):
        action = event['action']
        data = event['data']

        Person.objects.mongo_update_one(
            {'_id': ObjectId(data['id'])},
            {'$pull': {'resume.skills': {'name': data['name']}}}
        )

    def resume_update_expertise_rating(self, event):
        action = event['action']
        data = event['data']

        Person.objects.mongo_update_one(
            {
                '_id': ObjectId(data['id']),
                'resume.expertise.name': data['name']
            },
            {
                '$set': {
                    "resume.expertise.$.rating": data['rating']
                }
            }
        )


    def resume_delete_expertise(self, event):
        action = event['action']
        data = event['data']

        Person.objects.mongo_update_one(
            {'_id': ObjectId(data['id'])},
            {'$pull': {'resume.expertise': {'name': data['name']}}}
        )
